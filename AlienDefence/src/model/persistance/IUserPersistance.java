package model.persistance;

import toDo.User;

public interface IUserPersistance {

	User readUser(String username);

}